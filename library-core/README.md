Navigation-Core
===============

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Anavigation/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Anavigation/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:navigation-core:${DESIRED_VERSION}@aar"

## Elements ##
> Not available yet.